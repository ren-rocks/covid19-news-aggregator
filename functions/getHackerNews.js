const Parser = require('rss-parser');
const AWS = require('aws-sdk');
const _ = require('lodash');
const TableName = process.env.DYNAMODB_TABLE;

const getTodaysKey = () => (new Date()).getUTCDate() + '/' + ((new Date()).getUTCMonth() + 1) + '/' + (new Date()).getUTCFullYear();

const createResponse = (body = {}, statusCode = 200) => {
    return {
        statusCode,
        body: JSON.stringify(body, null, 2),
    };
};

const getStoriesFromHackerNews = async () => {
    const parser = new Parser();
    const feed = await parser.parseURL('https://hnrss.org/newest?q=coronavirus');

    let items = {};
    for (let i = 0; i < feed.items.length; i++) {
        let item = feed.items[i];
        if (item.link.indexOf('ycombinator') >= 0) {
            continue;
        }

        items[item.link] = {
            title: item.title,
            published: Date.parse(item.pubDate)
        };
    }

    return items;
};

const getCurrentItems = async () => {
    const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

    let items = {};
    const data = await dynamoDB.getItem({
        TableName,
        Key: {
            "date": {
                S: getTodaysKey()
            }
        },
    }).promise();
    if (typeof data.Item !== 'undefined') {
        items = JSON.parse(data.Item.data.S);
    }

    return items;
};

const setNewItems = async items => {
    const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    const params = {
        TableName,
        Item: {}
    };
    params.Item.date = {};
    params.Item.data = {};
    params.Item.date.S = getTodaysKey();
    params.Item.data.S = JSON.stringify(items);
    const res = await dynamoDB.putItem(params).promise();

    return res;
};

const getHackerNews = async event => {
    console.log('start');
    const hackerNewsStories = await getStoriesFromHackerNews().catch(err => {
        console.error(err);
        return createResponse({ 'success': false, err }, 500);
    });

    console.log(hackerNewsStories);
    console.log(`getting current items from table ${TableName}`);
    let items = await getCurrentItems().catch(err => {
        console.error(err);
        return createResponse({ 'success': false, err }, 500);
    });

    console.log(items);
    console.log('merging items');
    _.merge(items, hackerNewsStories);

    console.log('setting new items');
    console.log(items);
    const res = await setNewItems(items).catch(err => {
        console.error(err);
        return createResponse({ 'success': false, err }, 500);
    });

    if (res.statusCode >= 200 && res.statusCode < 300) {
        console.error(res);
        return createResponse({ 'success': false, res }, res.statusCode);
    }

    const success = { 'success': true, res };
    console.log(success);
    return createResponse(success);
};


module.exports.getHackerNews = getHackerNews;