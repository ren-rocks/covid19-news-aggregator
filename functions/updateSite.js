const AWS = require('aws-sdk');

const createResponse = (body = {}, statusCode = 200) => {
    return {
        statusCode,
        body: JSON.stringify(body, null, 2),
    };
};

const getIconTag = url => {
    let a = url.split('/');
    if (a.length < 3) {
        return '';
    }

    return `<img src="https://favicons.githubusercontent.com/${a[2]}" style="height:14px; margin: 0px 10px;" alt="${a[2]}">`;
};

const getTodaysKey = () => (new Date()).getUTCDate() + '/' + ((new Date()).getUTCMonth() + 1) + '/' + (new Date()).getUTCFullYear();

const createLinkHTMLTag = item => `<li style="margin-bottom: .5em;"><a class="text-info" href="${item.url}" target="_blank" >${item.title}</a >${getIconTag(item.url)}</li > `;

const updateSite = async event => {
    const fs = require('fs');
    const Handlebars = require('handlebars');
    const Bucket = process.env.S3BUCKET;
    const TableName = process.env.DYNAMODB_TABLE;

    console.log('start');
    const dynamoDB = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    const data = await dynamoDB.getItem({
        TableName,
        Key: {
            "date": {
                S: getTodaysKey()
            }
        },
    }).promise().catch(err => {
        console.error(err);
        return createResponse({ 'success': false, err }, 500);
    });

    if (typeof data.Item === 'undefined') {
        console.error(`no item for date ${getTodaysKey()} was found.`);
        return createResponse({ 'success': false, data }, 500);
    }

    let items = JSON.parse(data.Item.data.S);
    items = Object.keys(items).map(key => {
        items[key].url = key;
        return items[key];
    });
    _.remove(items, i => {
        return typeof i.title === 'undefined' || i.title === 'undefined';
    });

    items = _.orderBy(items, ['published'], 'desc');

    let markup = '';
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        markup += createLinkHTMLTag(item);

        if (i === 24) {
            break;
        }
    }

    if (markup === '') {
        console.error(`no items for date ${getTodaysKey()} were found and markup returned empty. not updating.`);
        return createResponse({ 'success': false, data }, 500);
    }

    const footer = `Last updated ${(new Date()).toUTCString()}`;
    const templateContent = await fs.readFileSync('./static/templates/index.html').toString();
    const template = Handlebars.compile(templateContent);
    const Body = template({ markup, footer });

    const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    const res = await s3.putObject({
        Body,
        Bucket,
        Key: 'index.html',
        ACL: 'public-read',
        ContentType: 'text/html; charset=UTF-8',
        Metadata: {
            'Content-Type': 'text/html'
        }
    }).promise().catch(err => {
        console.error(err);
        return createResponse({ 'success': false, err }, 500);
    });

    if (res.statusCode >= 200 && res.statusCode < 300) {
        console.error(res);
        return createResponse({ 'success': false, res }, res.statusCode);
    }

    const success = { 'success': true, res };
    console.log(success);
    return createResponse(success);
};

module.exports.updateSite = updateSite;