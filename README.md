# A Coronavirus News Aggregator

This is a serverless news aggregator for covid-19 related news running on AWS and cloudflare.

## installation
1. Run `npm i`
2. Edit `serverless.yml.example` custom variables and save it as `serverless.yml`
3. Run `sls deploy` **

** sometimes the serverless framework fails to deploy because it can't find an API endpoint. This can be fixed by going into your SLS dashboard and manually creating a service with the name added on the serverless.yml

## if you want to use cloudflare as your DNS registrar
4. install teraform
5. Run `cd terraform; terraform init`
6. Create a `terraform.tfvars` file and add the necessary variables. The endpoint should be the url of your newly formed cloudfront distribution.
7. Run `terraform apply`
8. Make sure that your aws account has a custom certificate approved within the certificate manager service.

## Description
The cloudwatch event automation should run every 15 minutes collecting news from the functions added in the serverless yml file. New sources can be added by copying one of the current functions and editing it to apply to the new source. The important piece of this automation is the way that the data is saved into the DynamoDB table holding all the articles.