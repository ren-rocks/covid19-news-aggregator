variable "zone_id" {
  type = string
}

variable "endpoint" {
  type = string
}

provider "cloudflare" {
  version = "~> 2.0"
}

resource "cloudflare_record" "corvid19" {
  zone_id = var.zone_id
  name    = "covid19"
  value   = var.endpoint
  type    = "CNAME"
  ttl     = 1
  proxied = true
}
